# Exécuter du code Python dans un navigateur avec WebAssembly

Voici un éditeur qui exécute du code Python dans un navigateur en utilisant
CodeMirror et WebAssembly. Ce dépôt repose sur ce [fork] (en faisant
l'impasse du serveur Flask).

## Comment l'utiliser ?

En utilisant les [pages] Framagit.


[fork]: https://github.com/amirtds/python_editor_wasm
[pages]: https://s2sio.frama.io/python_editor_wasm/
